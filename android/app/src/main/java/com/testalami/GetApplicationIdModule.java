package com.testalami;

import android.os.Build;

import android.provider.Settings;
import android.provider.Settings.Secure;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

public class GetApplicationIdModule extends ReactContextBaseJavaModule {

  GetApplicationIdModule(ReactApplicationContext context) {
    super(context);
  }

  public String getName() {
    return "GetApplicationIdModule";
  }

  @ReactMethod
  public void get(Promise promise) {
    try {
      String deviceId = android.provider.Settings.Secure.getString(
          getReactApplicationContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

      promise.resolve(deviceId);
    } catch (Exception e) {
      promise.reject("Device ID Not Found", e);
    }
  }
}
