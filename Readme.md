# How To Install and How To Run Application

## Requirements
- NodeJS ([Here](https://nodejs.org/en/download/))
- React Native ([How To Setup React Native](https://reactnative.dev/docs/environment-setup))
- XCode (If Want To Run App on IOS)
- Yarn (Optional [How To Install Yarn](https://classic.yarnpkg.com/lang/en/docs/install/))
- Emulator, Simulator or Device To Install App


## Running The Application
1. Clone this repo.
2. After Clone this Repo Run this on terminal 
```javascript
cd test-alami
// Or Change directory to where you clone it.
```
3. Run on Terminal
```javascript
// if using NPM
npm install

// if using yarn
yarn install
```
4. Then Run this on terminal (**This is Mandatory if You Want To Run on IOS**)
```javascript
npx pod-install
```

5. After `npm install` and `pod install` Run your `emulator` then run this
```javascript
// on Android
npm run android
// or 
yarn android


// on IOS
npm run ios
// or 
yarn ios
//
```

6. (Optional Method for Run on IOS) On IOS you can open the Xcode, and open `testalami.xcworkspace`, then select emulator you want to use, then click `run` icon to install the app.

## Android APK
If you want to test without build it first, you can download the apk here [Download APK](https://drive.google.com/file/d/1qjzfnVZIlDtCqgsDmTYC3OnsBKCfzuHY/view?usp=sharing)

## Screenshots
| IOS     | ANDROID |
| :---:   | :---:   |
| ![IOS 1](./screenshots/iphone1.png) | ![Android 1](./screenshots/android1.png) |
| ![IOS 2](./screenshots/iphone2.png) | ![Android 2](./screenshots/android2.png) |
| ![IOS 3](./screenshots/iphone3.png) | ![Android 3](./screenshots/android3.png) |
| ![IOS 4](./screenshots/iphone4.png) | ![Android 4](./screenshots/android4.png) |
| ![IOS 5](./screenshots/iphone5.png) | ![Android 5](./screenshots/android5.png) |
| ![IOS 6](./screenshots/iphone6.png) | ![Android 6](./screenshots/android6.png) | 