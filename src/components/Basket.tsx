import React, {FunctionComponent} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {formatCurrency} from '../helper/Format';
import BasketItem, {BasketItemProps} from './BasketItem';
import Touchable from './Touchable';

interface BasketProps {
  cart: Array<BasketItemProps>;
  onPressCheckout: (total: number) => void;
  onPressAdd: (index: number) => void;
  onPressSub: (index: number) => void;
  onPressRemove: (index: number) => void;
}

const Basket: FunctionComponent<BasketProps> = ({
  cart,
  onPressCheckout,
  onPressSub,
  onPressAdd,
  onPressRemove,
}) => {
  const total = cart.reduce(
    (prev, current) => prev + current.price * current.quantity,
    0,
  );

  return (
    <View style={style.wrapper}>
      <Text style={style.topText}>Cart</Text>
      <FlatList
        style={style.container}
        data={cart}
        extraData={cart}
        ListEmptyComponent={() => (
          <Text style={style.emptyText}>Cart is Empty.</Text>
        )}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <BasketItem
            onPressSub={() => onPressSub(index)}
            onPressRemove={() => onPressRemove(index)}
            onPressAdd={() => onPressAdd(index)}
            item={item}
          />
        )}
      />
      <View style={style.bottomBar}>
        <Text style={style.totalText}>
          Total: {cart.length < 1 ? '-' : formatCurrency(total)}
        </Text>
        <Touchable
          disabled={cart.length < 1}
          onPress={() => onPressCheckout(total)}
          style={cart.length < 1 ? style.buttonDisabled : style.button}>
          <Text style={style.buttonText}>Checkout</Text>
        </Touchable>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  wrapper: {
    width: '100%',
    flex: 1,
  },
  container: {
    width: '100%',
    flex: 1,
    padding: 16,
    marginTop: 12,
    backgroundColor: 'white',
  },
  bottomBar: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingVertical: 24,
    paddingHorizontal: 24,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.4,
    elevation: 4,
  },
  totalText: {
    fontSize: 14,
    color: 'black',
  },
  topText: {
    marginTop: 24,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'black',
  },
  emptyText: {
    marginTop: 24,
    fontSize: 16,
    textAlign: 'center',
    color: 'black',
  },
  button: {
    borderRadius: 8,
    backgroundColor: 'green',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  buttonDisabled: {
    borderRadius: 8,
    backgroundColor: 'gray',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  buttonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default Basket;
