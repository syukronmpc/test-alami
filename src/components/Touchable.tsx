import React, {FunctionComponent} from 'react';
import {
  GestureResponderEvent,
  Platform,
  StyleProp,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';

interface TouchableProps {
  disabled?: boolean;
  onPress: (event: GestureResponderEvent) => void;
  children: React.ReactNode;
  style?: StyleProp<ViewStyle>;
}

const Touchable: FunctionComponent<TouchableProps> = ({
  disabled,
  onPress,
  children,
  style,
}) => {
  if (Platform.OS === 'ios') {
    return (
      <TouchableOpacity disabled={disabled} onPress={onPress} style={style}>
        {children}
      </TouchableOpacity>
    );
  }

  return (
    <TouchableNativeFeedback
      useForeground
      background={TouchableNativeFeedback.Ripple('#FFFFFF80', false, 100)}
      disabled={disabled}
      onPress={onPress}>
      <View style={style}>{children}</View>
    </TouchableNativeFeedback>
  );
};

Touchable.defaultProps = {
  style: {},
  disabled: false,
};

export default Touchable;
