import React, {FunctionComponent} from 'react';
import {Image, ImageSourcePropType, StyleSheet, Text, View} from 'react-native';
import {formatCurrency} from '../helper/Format';
import Touchable from './Touchable';

export interface BasketItemProps {
  name: string;
  price: number;
  quantity: number;
  image: ImageSourcePropType;
}

interface BasketItemListProps {
  item: BasketItemProps;
  onPressAdd: () => void;
  onPressSub: () => void;
  onPressRemove: () => void;
}

const BasketItem: FunctionComponent<BasketItemListProps> = ({
  item,
  onPressAdd,
  onPressRemove,
  onPressSub,
}) => {
  const isOnlyOne = item.quantity <= 1;
  const isMaxValue = item.quantity >= 99;

  return (
    <View style={style.wrapper}>
      <View style={style.container}>
        <Image resizeMode="contain" style={style.image} source={item.image} />
        <View style={style.textContainer}>
          <Text style={style.textName}>{item.name}</Text>
          <Text style={style.textPrice}>{formatCurrency(item.price)}</Text>
          <Text style={style.textQuantity}>Quantity: {item.quantity}</Text>
          <Text style={style.textPrice}>
            Total: {formatCurrency(item.quantity * item.price)}
          </Text>
          <View style={style.row}>
            <Touchable
              disabled={isMaxValue}
              style={isMaxValue ? style.buttonDisabled : style.buttonAdd}
              onPress={onPressAdd}>
              <Text style={style.buttonText}>+</Text>
            </Touchable>
            <Touchable
              disabled={isOnlyOne}
              style={isOnlyOne ? style.buttonDisabled : style.buttonSub}
              onPress={onPressSub}>
              <Text style={style.buttonText}>-</Text>
            </Touchable>
            <Touchable style={style.buttonRemove} onPress={onPressRemove}>
              <Text style={style.buttonText}>Remove</Text>
            </Touchable>
          </View>
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  wrapper: {
    width: '100%',
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderRadius: 12,
  },
  textContainer: {
    padding: 8,
    flex: 1,
    flexDirection: 'column',
  },
  textName: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'black',
  },
  textPrice: {
    marginTop: 4,
    fontSize: 12,
    color: 'black',
  },
  textQuantity: {
    fontSize: 12,
    color: 'black',
  },
  image: {
    width: 100,
    height: 100,
  },
  row: {
    marginTop: 8,
    flexDirection: 'row',
  },
  buttonAdd: {
    backgroundColor: 'green',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 12,
    marginRight: 12,
  },
  buttonSub: {
    backgroundColor: 'orange',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 12,
    marginRight: 12,
  },
  buttonDisabled: {
    backgroundColor: 'gray',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 12,
    marginRight: 12,
  },
  buttonRemove: {
    borderRadius: 12,
    backgroundColor: 'red',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export default BasketItem;
