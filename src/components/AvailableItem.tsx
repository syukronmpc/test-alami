import React, {FunctionComponent} from 'react';
import {Image, ImageSourcePropType, StyleSheet, Text, View} from 'react-native';
import {formatCurrency} from '../helper/Format';
import {BasketItemProps} from './BasketItem';
import Touchable from './Touchable';

export interface AvailableItemProps {
  name: string;
  price: number;
  quantity: number;
  image: ImageSourcePropType;
}

interface AvailableItemListProps {
  item: AvailableItemProps;
  onPressAdd: (item: BasketItemProps) => void;
}

const AvailableItem: FunctionComponent<AvailableItemListProps> = ({
  item,
  onPressAdd,
}) => {
  return (
    <View style={style.wrapper}>
      <View style={style.container}>
        <Image resizeMode="contain" style={style.image} source={item.image} />
        <View style={style.textContainer}>
          <Text style={style.textName}>{item.name}</Text>
          <Text style={style.textPrice}>{formatCurrency(item.price)}</Text>
          <Touchable style={style.button} onPress={() => onPressAdd(item)}>
            <Text style={style.buttonText}>Add</Text>
          </Touchable>
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  wrapper: {
    width: '100%',
    paddingHorizontal: 16,
    marginBottom: 8,
  },
  container: {
    backgroundColor: 'white',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderRadius: 12,
  },
  textContainer: {
    flex: 1,
    padding: 8,
    flexDirection: 'column',
  },
  textName: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'black',
  },
  textPrice: {
    fontSize: 12,
    marginTop: 2,
    color: 'black',
  },
  textQuantity: {
    fontSize: 12,
    color: 'black',
  },
  image: {
    width: 100,
    height: 100,
  },
  button: {
    marginTop: 8,
    alignSelf: 'flex-end',
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 24,
    marginRight: 12,
    paddingVertical: 8,
    borderRadius: 12,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export default AvailableItem;
