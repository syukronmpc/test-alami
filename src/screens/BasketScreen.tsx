import React, {useRef, useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {Modalize} from 'react-native-modalize';
import {SafeAreaView} from 'react-native-safe-area-context';
import SimpleToast from 'react-native-simple-toast';
import images from '../../assets/images';
import AvailableItem, {AvailableItemProps} from '../components/AvailableItem';
import Basket from '../components/Basket';
import {BasketItemProps} from '../components/BasketItem';
import Touchable from '../components/Touchable';
import {formatCurrency} from '../helper/Format';

const availableItems: Array<AvailableItemProps> = [
  {
    name: 'Apple',
    price: 8000,
    quantity: 1,
    image: images.apple,
  },
  {
    name: 'Guava',
    price: 7000,
    quantity: 1,
    image: images.guava,
  },
  {
    name: 'Orange',
    price: 5000,
    quantity: 1,
    image: images.orange,
  },
  {
    name: 'Watermelon',
    price: 40000,
    quantity: 1,
    image: images.watermelon,
  },
];

const BasketScreen = () => {
  const dimension = useWindowDimensions();

  const [cart, setCart] = useState<Array<BasketItemProps>>([]);
  const modalBasket = useRef<Modalize>(null);

  const onPressAdd = (index: number) => {
    const cartNew = cart.slice();
    cartNew[index].quantity += 1;
    setCart(cartNew);
  };

  const onPressSub = (index: number) => {
    const cartNew = cart.slice();
    cartNew[index].quantity -= 1;
    setCart(cartNew);
  };

  const onPressremove = (index: number) => {
    const cartNew = cart.slice();
    SimpleToast.showWithGravity(
      `Item ${cartNew[index].name} Successfully removed from cart.`,
      SimpleToast.SHORT,
      SimpleToast.TOP,
    );
    cartNew.splice(index, 1);
    setCart(cartNew);
  };

  const onPressCheckout = (total: number) => {
    SimpleToast.showWithGravity(
      `Successfully checkout and pay ${formatCurrency(total)}.`,
      SimpleToast.SHORT,
      SimpleToast.TOP,
    );

    modalBasket.current?.close();
    setCart([]);
  };

  const filteredAvailableItems = availableItems
    .slice()
    .filter(i => !cart.some(d => d.name === i.name));

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={style.rowContainer}>
        <Touchable
          onPress={() => modalBasket.current?.open()}
          style={style.button}>
          <Text style={style.buttonText}>Cart</Text>
        </Touchable>
        {/* Badge */}
        {cart.length > 0 && (
          <View style={style.badge}>
            <Text style={style.badgeText}>{cart.length}</Text>
          </View>
        )}
      </View>
      <FlatList
        data={filteredAvailableItems}
        ListEmptyComponent={() => (
          <Text style={style.emptyText}>List is Empty.</Text>
        )}
        renderItem={({item}) => (
          <AvailableItem
            onPressAdd={itemAdd => {
              const newCart = cart.slice();
              newCart.push(itemAdd);
              setCart(newCart);
              SimpleToast.showWithGravity(
                `Item ${itemAdd.name} Successfully added to cart.`,
                SimpleToast.SHORT,
                SimpleToast.TOP,
              );
            }}
            item={item}
          />
        )}
      />
      <Modalize
        scrollViewProps={{
          scrollEnabled: false,
        }}
        customRenderer={
          <View style={{flex: 1}}>
            <Basket
              onPressAdd={onPressAdd}
              onPressRemove={onPressremove}
              onPressSub={onPressSub}
              cart={cart}
              onPressCheckout={onPressCheckout}
            />
          </View>
        }
        disableScrollIfPossible
        modalStyle={style.modalContainer}
        handlePosition="inside"
        modalHeight={
          Platform.OS === 'ios'
            ? dimension.height - 300
            : dimension.height - 200
        }
        ref={modalBasket}
      />
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  rowContainer: {
    paddingHorizontal: 16,
    marginTop: 16,
    marginBottom: 24,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'flex-end',
  },
  button: {
    backgroundColor: 'blue',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 8,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
  emptyText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalContainer: {
    backgroundColor: 'white',
  },
  badge: {
    width: 16,
    height: 16,
    position: 'absolute',
    top: -6,
    right: 10,
    borderRadius: 100,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeText: {
    color: 'white',
    fontSize: 12,
  },
});

export default BasketScreen;
