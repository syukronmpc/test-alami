import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {Bar} from 'react-native-progress';
import Touchable from '../components/Touchable';

const LoadingScreen = () => {
  const [progress, setProgress] = useState(0.0);
  const [stopLoading, setStopLoading] = useState(false);

  useEffect(() => {
    const interval = setInterval(() => {
      if (!stopLoading && progress < 1.0) {
        setProgress(progress + 0.01);
      } else {
        if (progress >= 1.0) {
          setProgress(1.0);
        }
      }
    }, 500);

    return () => clearInterval(interval);
  }, [progress, stopLoading]);

  return (
    <SafeAreaView style={style.safearea}>
      <View style={style.container}>
        <Text style={style.textHead}>Loading</Text>
        <Text style={style.textHeadBold}>{(progress * 100).toFixed(0)}%</Text>
        <Text style={style.textHead}>{stopLoading}</Text>
        <TouchableWithoutFeedback
          onPressOut={() => setStopLoading(false)}
          onPressIn={() => setStopLoading(true)}>
          <Bar color="blue" progress={progress} height={32} width={300} />
        </TouchableWithoutFeedback>
        <Text style={style.textHead}>Hold Loading To Pause Progress.</Text>
        {progress >= 1.0 && (
          <Touchable style={style.button} onPress={() => setProgress(0.0)}>
            <Text style={style.buttonText}>Reset</Text>
          </Touchable>
        )}
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  safearea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 16,
  },
  container: {
    borderRadius: 18,
    paddingVertical: 32,
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  textHead: {
    fontSize: 14,
    color: 'black',
  },
  textHeadBold: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'black',
  },
  textId: {
    marginTop: 16,
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
  },
  button: {
    marginTop: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 8,
    backgroundColor: 'red',
  },
  buttonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default LoadingScreen;
