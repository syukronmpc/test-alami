import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  NativeModules,
  Platform,
  StyleSheet,
} from 'react-native';
import Touchable from '../components/Touchable';

const DeviceIdScreen = ({navigation}: any) => {
  const [deviceId, setDeviceId] = useState('');
  const {GetApplicationIdModule} = NativeModules;

  const getIdFromNativeModules = async (): Promise<void> => {
    try {
      const id = await GetApplicationIdModule.get();
      setDeviceId(id);
      return;
    } catch (err) {
      setDeviceId('Error');
      console.log(err);
    }
  };

  useEffect(() => {
    getIdFromNativeModules();
  }, []);

  return (
    <SafeAreaView style={style.safearea}>
      <View style={style.container}>
        <Text style={style.textHead}>
          {Platform.OS === 'android' ? 'Android' : 'IOS'} Device ID
        </Text>
        <Text style={style.textId}>{deviceId}</Text>
      </View>

      <Touchable
        style={style.button}
        onPress={() => navigation.navigate('basket')}>
        <Text style={style.buttonText}>Basket Screen</Text>
      </Touchable>
      <Touchable
        style={style.button}
        onPress={() => navigation.navigate('loading')}>
        <Text style={style.buttonText}>Loading Screen</Text>
      </Touchable>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  safearea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 16,
  },
  container: {
    borderRadius: 18,
    paddingVertical: 32,
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blue',
  },
  textHead: {
    fontSize: 14,
    color: 'white',
  },
  textId: {
    marginTop: 16,
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
  },
  button: {
    marginTop: 32,
    paddingHorizontal: 16,
    paddingVertical: 16,
    borderRadius: 16,
    backgroundColor: 'green',
  },
  buttonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default DeviceIdScreen;
