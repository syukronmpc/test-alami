import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import DeviceIdScreen from './DeviceIdScreen';
import BasketScreen from './BasketScreen';
import LoadingScreen from './LoadingScreen';

const Stack = createNativeStackNavigator();

export default function ScreenWrapper() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="device-id">
        <Stack.Screen
          name="device-id"
          options={{title: 'Device ID Screen'}}
          component={DeviceIdScreen}
        />
        <Stack.Screen
          name="basket"
          options={{title: 'Basket Screen'}}
          component={BasketScreen}
        />
        <Stack.Screen
          name="loading"
          options={{title: 'Loading Screen'}}
          component={LoadingScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
