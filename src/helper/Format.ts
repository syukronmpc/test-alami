export const formatCurrency = (
  nominal: any,
  currency = 'Rp',
  separator = '.',
) => {
  let nominalFormatted = '';

  if (Number.isNaN(nominalFormatted)) {
    nominalFormatted = '';
  }

  if (nominalFormatted || nominalFormatted === '' || nominal === 0) {
    return (
      currency + nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)
    );
  }

  return '';
};
