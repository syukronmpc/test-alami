#import "RCTGetApplicationIdModule.h"
#import <React/RCTLog.h>

@implementation RCTGetApplicationIdModule

// To export a module named GetApplicationIdModule
RCT_EXPORT_MODULE(GetApplicationIdModule);
RCT_EXPORT_METHOD(get: (RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  NSUUID *deviceId = [UIDevice currentDevice].identifierForVendor;
  
  RCTLogInfo(@"Pretending to create an deviceId %@", deviceId);
  
  if (deviceId) {
    resolve(deviceId.UUIDString);
  } else {
    reject(@"Device ID Not found", @"No Device Id Returned", nil);
  }
}

@end
