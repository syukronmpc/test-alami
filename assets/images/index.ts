export default {
  apple: require('./apple.jpeg'),
  guava: require('./guava.jpeg'),
  orange: require('./orange.jpeg'),
  watermelon: require('./watermelon.jpeg'),
};
