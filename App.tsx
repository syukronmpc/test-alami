import React from 'react';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

import ScreenWrapper from './src/screens/ScreenWrapper';

const App = () => {
  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <ScreenWrapper />
    </GestureHandlerRootView>
  );
};

export default App;
